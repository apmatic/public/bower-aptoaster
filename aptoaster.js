'use strict';

angular.module('ap.toaster', ['ng', 'pascalprecht.translate', 'toaster']).factory('$toaster', function ($translate, toaster){
  return {
    pop: function (event, title, text, time, option, fun) {
      $translate([title, text]).then(function (translations){
        toaster.pop(event, translations[title], translations[text], time, option, fun);
      });
    },
    clear: function(){
      toaster.clear();
    }
  };
});
